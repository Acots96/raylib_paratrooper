UOC - Graphic Programming - Aleix Cots Molina

PEC1: Paratrooper

*(source files are stored in "game/src/" folder, resources in "resources" folder)*

Link to video (in folder "Portfolio/Raylib(C++)/Paratrooper2D"): https://drive.google.com/drive/folders/1f19dK-otYpjN3iWk3WBaSh5HfGdSFySk?usp=sharing

===========================================================

The whole logic is split in two parts.

---------------------

On one hand, the program has a little "engine" implemented to do the heavy stuff that a game engine usually does, so the Paratrooper
game logic can be decoupled (mostly) from this. This little engine only implements the basic logic needed with the following classes:

	- Engine: Main static class. Allows to:
		- Create or destroy an instance of the given game.
		- Instantiate or destroy a new actor.
		- Update all the actors in the game, which includes: calling their start/update and draw methods so they can update themselves and implement
		  their own drawing; check all the collisions between actors and call their collision method so they can decide what 
		  to do (like Unity's OnCollisionStay/OnTriggerStay methods); and remove the actors that were destroyed (marked as inactive).
		  
	- Actor: Base abstract class that a class must implement if it is intended to be an actor of the game. It has five virtual methods that
	  the derived class can implement if needed:
		- Start is called the first time that an actor is updated.
		- Update is called from the second time on, until it is destroyed.
		- Draw allows the actor decide what needs to be drawn in the screen.
		- GetBody allows the subclass to define a shape for the actor so Engine can check the collisions.
		- CollisionWith(Actor) notifies the actor about a collision with another actor.
		
	- ActorState: Helper class inside of Engine used as a wrapper for all the actors to provide further information about the actor only known
	  by the engine. In particular, it contains two Boolean:
		- isAlive indicates whether the actor can be definitely removed or not.
		- isStarted indicates if its Start method has been called.
		
---------------------

On the other hand, the logic of the Paratrooper game is divided as follows:

	- Paratrooper: Main class of the game. Works as a FSM of screens to switch between them.
	
	- Screens system:
		- ScreensManager: Interface that the manager of the screens (Paratrooper) must implement so they can ask to switch from one to another.
		- Screen: Base class that all screens must implement.
		- LogoScreen: Shows an image for a few seconds, then asks the manager to switch to TitleScreen.
		- TitleScreen: Acts as a main menu which allows the player to switch to OptionsScreen or start the game (GameplayScreen).
		- OptionsScreen: Shows a little information about the game and how to play. Switches to TitleScreen when the player hits "O".
		- GameplayScreen: Starts the game and updates the UI information. Eventually, when the game is over, will switch to EndingScreen.
		- EndingScreen: Last screen of the game. Will allow the user to switch to TitleScreen or OptionsScreen.		
	
	- GameManager: Main class of the gameplay part. Instantiates the player, the ground, the helicopters and keeps a track of all the "non-static"
	  actors so they will be destroyed when the gameplay is over.
	  
	- Player: Receives the input and behaves accordingly, rotating the turret and instantiating the shots.
	
	- Ground: Only waits to be notified by soldiers that they have collided and updates its "Landed" counter.
	
	- Shot: Class used to represent a shot. Given an initial position, moves the body with a certain velocity (Vector2), checks the collisions 
	  with any enemy (Soldier or Helicopter) to tell him and increases the player's score. Destroys itself after leaving the screen.
	  
	- Helicopter: Moves the helicopter horizontally across the screen with a certain velocity and randomly generates soldiers. Destroys itself after leaving the screen.
	  
	- Soldier: Moves the body towards the ground, simulating the fall. Unlike the helicopter or the shot, when a soldier is hit it does not disappear, 
	  instead it changes its image, becomes unreachable for X time (1 second) and then disappears.
	 
	- TexturesManager: Static class that holds all the textures used in the game. It also implements Load and Unload methods to load and unload
	  the textures whenever they are called (it is recommended to call them before the game starts and after it is over).
	  Textures used: Logo, Title, BodyPlayer, Turret, Left/RightHelicopter, Soldier, Dead (soldier).
	  
	- AudioManager: Static class that holds all the sounds and music used in the game. It also implements Load and Unload methods like TexturesManager.
	  Aside from the textures, not all the sounds are the same, a few are Music type because it allows looping.
	  Sounds used: Title, Shot, Explosion (helicopter), Jump, Land, EnemyDie (soldier) and Loose.
	  Music used: Game (song/melody during the game), Rotate (turret) and Helicopter.
	  (both textures and sounds are stored in the resources folder)

---------------------
	
Raylib functions used:

	- Drawing:
		- Basic initializations/deactivations: InitWindow/CloseWindow, SetTargetFPS and LoadTexture/UnloadTexture.
		- BeginDrawing and ClearBackground called before drawing the actors, and EndDrawing called right after.
		- DrawTexturePro to draw the images instead of DrawTexture because it allows to rescale or rotate the image (player's turret).
		- DrawText to draw any text.
			- MeasureText to get the width of a text so it can be centered horizontally.
		- DrawCircle to draw the shots instead of an image.
		
	- Sounds:
		- Basic initializations/deactivations: InitAudioDevice/CloseAudioDevice, LoadSound/UnloadSound and 
		  LoadMusicStream/UnloadMusicStream.
		- PlaySound, SetSoundPitch and StopSound.
		- PlayMusicStream, SetMusicVolume, UpdateMusicStream and StopMusicStream to manage a music field.
		
	- CheckCollisionRecs to check if two Rectangle collide.
	
---------------------

*Look over the classes and methods for further information about the implementation*
	
---------------------

To improve:

    - Play around a little more with Raylib's functions because there is an insane amount of them and this little project only uses a few.
	
	- Add a "layer" value to Actor so they will be drawn in the correct order (the first actor to be drawn will be the one with the lowest layer
	  and the last will be the one with the highest layer).
	  
	- Implement a "Scenes" system to make different lists of Actor (one for each scene) instead of one single big list of actors. It would allow
	  to make a cleaner way to switch from one screen to another without the need of an actor keeping a track of the rest (like Player, who holds
	  a list of Shot to destroy them when the player loses).
	  
	- Improve my C++ knowledge like using smart pointers instead of raw pointers (I tried them at first, but did not work).