#pragma once
#include <iostream>
#include <raylib.h>
#include "Screen.h"
#include "Paratrooper.h"

// Screen containing the main information about the game.
class OptionsScreen : public Screen
{
	private:
		const char* _text;
		Vector2 _textPosition;

	public:
		OptionsScreen(Paratrooper*);
		void Update() override;
		void Draw() override;
};
