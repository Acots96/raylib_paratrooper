#include "EndingScreen.h"
#include "AudioManager.h"

EndingScreen::EndingScreen(Paratrooper* manager) : Screen(manager)
{
	_text = "You loose... :("
		"\n\nPress Enter for Title\n"
		"Press 'O' for Options";
	int textWidth = MeasureText(_text, _width / 45);
	_textPosition = { (float)(_width / 2 - textWidth / 2), _height * 0.1f };

	PlaySound(AudioManager::Loose);
}

void EndingScreen::Update()
{
	if (IsKeyReleased(KEY_O))
	{
		_manager->SwitchTo("Options");
	}
	else if (IsKeyReleased(KEY_ENTER))
	{
		_manager->SwitchTo("Title");
	}
}

void EndingScreen::Draw()
{
	DrawText(_text, (int)_textPosition.x, (int)_textPosition.y, _width / 45, WHITE);
}

EndingScreen::~EndingScreen()
{
	StopSound(AudioManager::Loose);
}
