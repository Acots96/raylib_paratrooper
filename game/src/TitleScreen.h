#pragma once
#include <iostream>
#include <raylib.h>
#include "Screen.h"
#include "Paratrooper.h"

// Screen show after the LogoScreen,
// we could say is the main menu
class TitleScreen : public Screen
{
	private:
		Texture2D _image;
		float _imageAspectRatio;
		Rectangle _source;
		Rectangle _dest;
		//
		const char* _text;

	public:
		TitleScreen(Paratrooper*);
		void Update() override;
		void Draw() override;
		~TitleScreen() override;
};
