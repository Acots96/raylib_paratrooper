#include "Shot.h"
#include <typeinfo>
#include <iostream>
#include "Helicopter.h"
#include "Soldier.h"
#include "Paratrooper.h"
#include "AudioManager.h"
#include "GameManager.h"

Shot::Shot(Vector2 startPos, Vector2 velocity, float shotRadius, Player* shooter) : Actor()
{
	_velocity = velocity;
	_size = shotRadius;

	_body = {
		startPos.x - _size / 2,
		startPos.y - _size / 2,
		_size,
		_size
	};

	_shooter = shooter;

	float min = 0.7f;
	float max = 1.3f;
	SetSoundPitch(AudioManager::Shot, min + (float)rand() / RAND_MAX * (max - min));
	PlaySound(AudioManager::Shot);

	GameManager::AddActor(this);
}

void Shot::Update()
{
	// This actor travels along the screen until hits another actor,
	// but if it doesn't, its position must be checked so it can be 
	// destroyed after leaving the screen
	if (_body.x + _body.width < 0 || _body.y + _body.height < 0 ||
		_body.x > GetRenderWidth() || _body.y > GetRenderHeight())
	{
		Engine::Destroy(this);
	}
	else
	{
		_body.x += _velocity.x * GetFrameTime();
		_body.y += _velocity.y * GetFrameTime();
	}
}

void Shot::Draw()
{
	DrawCircle(_body.x, _body.y, _size, SKYBLUE);
}

Rectangle Shot::GetBody()
{
	return _body;
}

void Shot::CollisionWith(Actor* actor)
{
	if (auto h = dynamic_cast<Helicopter*>(actor))
	{
		float min = 0.5f;
		float max = 1.5f;
		SetSoundPitch(AudioManager::Explosion, min + (float)rand() / RAND_MAX * (max - min));
		PlaySound(AudioManager::Explosion);
		h->Die();
		_shooter->IncreaseScore(GameManager::Scores[GameManager::Enemy::Helicopter]);
		Engine::Destroy(this);
	}
	else if (auto s = dynamic_cast<Soldier*>(actor))
	{
		// Soldiers do not disappear instantly like the helicopters do,
		// instead they replace their image and become unreachable,
		// so it needs to check if the soldier is still alive before killing it
		if (!s->IsDead())
		{
			s->Die();
			_shooter->IncreaseScore(GameManager::Scores[GameManager::Enemy::Soldier]);
			Engine::Destroy(this);
		}
	}
}

Shot::~Shot()
{
	GameManager::RemoveActor(this);
	StopSound(AudioManager::Shot);
}