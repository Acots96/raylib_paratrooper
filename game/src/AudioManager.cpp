#include "AudioManager.h"

Sound AudioManager::Title;
Music AudioManager::Game;
Music AudioManager::Rotate;
Sound AudioManager::Shot;
Music AudioManager::Helicopter;
Sound AudioManager::Explosion;
Sound AudioManager::Jump;
Sound AudioManager::Land;
Sound AudioManager::EnemyDie;
Sound AudioManager::Loose;

void AudioManager::LoadSounds()
{
	Title = LoadSound("resources/Title.ogg");
	Game = LoadMusicStream("resources/Game.mp3");
	Game.looping = true;
	Rotate = LoadMusicStream("resources/Rotate.wav");
	Rotate.looping = true;
	Shot = LoadSound("resources/Shot.ogg");
	Helicopter = LoadMusicStream("resources/Helicopter.ogg");
	Helicopter.looping = true;
	Jump = LoadSound("resources/Jump.ogg");
	Land = LoadSound("resources/Land.wav");
	Explosion = LoadSound("resources/Explosion.wav");
	EnemyDie = LoadSound("resources/EnemyDie.wav");
	Loose = LoadSound("resources/Loose.wav");
}

void AudioManager::UnloadSounds()
{
	UnloadSound(Title);
	UnloadMusicStream(Rotate);
	UnloadSound(Shot);
	UnloadMusicStream(Helicopter);
	UnloadSound(Jump);
	UnloadSound(Land);
	UnloadSound(EnemyDie);
	UnloadSound(Loose);
}
