#include "Soldier.h"
#include "TexturesManager.h"
#include <iostream>
#include "Shot.h"
#include "Ground.h"
#include "AudioManager.h"
#include "GameManager.h"

Soldier::Soldier(Vector2 startPos, float speed) : Actor()
{
	_body = TexturesManager::Soldier;
	_position = startPos;
	_speed = abs(speed);

	_bodySource = { 0, 0, (float)_body.width, (float)_body.height };
	_bodyDest = { _position.x, _position.y, (float)_body.width, (float)_body.height };
	
	_isDead = false;
	_deadTime = 1;

	PlaySound(AudioManager::Jump);

	GameManager::AddActor(this);
}

void Soldier::Update()
{
	if (_isDead)
	{
		if (_deadTime > 0)
		{
			_deadTime -= GetFrameTime();
			if (_deadTime <= 0)
			{
				Engine::Destroy(this);
			}
		}
	}
	else
	{
		_position.y += _speed * GetFrameTime();
		_bodyDest = { _position.x, _position.y, (float)_body.width, (float)_body.height };
	}
}

void Soldier::Draw()
{
	DrawTexturePro(_body, _bodySource, _bodyDest, { 0, 0 }, 0, WHITE);
}

Rectangle Soldier::GetBody()
{
	// Return an "empty" body if it is dead so no collisions will be performed against it
	return _isDead ? Rectangle{0, 0, 0, 0} : _bodyDest;
}

void Soldier::CollisionWith(Actor* actor)
{
	// Cannot fall to the ground if it is dead
	if (!_isDead)
	{
		if (auto g = dynamic_cast<Ground*>(actor))
		{
			PlaySound(AudioManager::Land);
			g->IncreaseLanded();
			_deadTime = -1;
			_isDead = true;
		}
	}	
}

void Soldier::Die()
{
	if (!_isDead)
	{
		_body = TexturesManager::Dead;
		_bodySource = { 0, 0, (float)_body.width, (float)_body.height };
		_bodyDest = { _position.x, _position.y, (float)_body.width, (float)_body.height };
		_isDead = true;
		float min = 0.5f;
		float max = 2.0f;
		SetSoundPitch(AudioManager::EnemyDie, min + (float)rand() / RAND_MAX * (max - min));
		PlaySound(AudioManager::EnemyDie);
	}
}

bool Soldier::IsDead()
{
	return _isDead;
}

Soldier::~Soldier()
{
	StopSound(AudioManager::Jump);
	StopSound(AudioManager::EnemyDie);
	GameManager::RemoveActor(this);
}
