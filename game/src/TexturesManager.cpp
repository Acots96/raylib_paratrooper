#include "TexturesManager.h"

Texture2D TexturesManager::Logo;
Texture2D TexturesManager::Title;
Texture2D TexturesManager::BodyPlayer;
Texture2D TexturesManager::Turret;
Texture2D TexturesManager::LeftHelicopter;
Texture2D TexturesManager::RightHelicopter;
Texture2D TexturesManager::Soldier;
Texture2D TexturesManager::Dead;

void TexturesManager::LoadTextures()
{
	Logo = LoadTexture("resources/Logo.png");
	Title = LoadTexture("resources/Title.png");
	BodyPlayer = LoadTexture("resources/PlayerBody.png");
	Turret = LoadTexture("resources/Turret.png");
	LeftHelicopter = LoadTexture("resources/Helicopter_Left.png");
	RightHelicopter = LoadTexture("resources/Helicopter_Right.png");
	Soldier = LoadTexture("resources/Soldier.png");
	Dead = LoadTexture("resources/Dead.png");
}

void TexturesManager::UnloadTextures()
{
	UnloadTexture(Logo);
	UnloadTexture(Title);
	UnloadTexture(BodyPlayer);
	UnloadTexture(Turret);
	UnloadTexture(LeftHelicopter);
	UnloadTexture(RightHelicopter);
	UnloadTexture(Soldier);
	UnloadTexture(Dead);
}
