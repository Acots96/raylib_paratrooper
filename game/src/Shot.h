#pragma once
#include "Actor.h"
#include <raylib.h>
#include <memory>
#include "Player.h"

using namespace std;

// Class representing the projectile launched by the player
class Shot : public Actor
{
	private:
		Vector2 _velocity;
		float _size;
		Rectangle _body;
		//
		Player* _shooter;

	public:
		Shot(Vector2 startPos, Vector2 velocity,
			float shotRadius, Player* shooter);
		void Update() override;
		void Draw() override;
		Rectangle GetBody() override;
		void CollisionWith(Actor*) override;
		~Shot() override;
};

