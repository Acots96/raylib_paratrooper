#pragma once
#include <raylib.h>
#include "Actor.h"
#include <list>
#include <memory>
#include <map>
#include "Player.h"
#include "Ground.h"

// Main manager of the paratrooper game,
// which aims to hold the player, the ground
// and generate the helicopters.
// "Temporary" actors like shots, helicopters or 
// soldiers must be added using AddActor 
// (besides Engine::Instantiate) so the manager
// can remove them when the gameplay screen is over.
class GameManager : public Actor
{
	public:
		enum State { None, Playing, Win, Loose };
		enum Enemy { Helicopter, Soldier };
		static map<Enemy, int> Scores;

	private:
		float _minTimeHelis;
		float _maxTimeHelis;
		float _deltaTime;
		float _helicopterSpeed;
		//
		Rectangle _screen;
		Player* _player;
		Ground* _ground;
		State _state;
		//
		static list<Actor*> _actors;
		//
		int _maxLanded;

	public:
		GameManager();
		void Update() override;
		~GameManager() override;
		int GetScore();
		int GetLanded();
		State GetState();
		void SetState(State);
		//
		static void AddActor(Actor*);
		static void RemoveActor(Actor*);
};
