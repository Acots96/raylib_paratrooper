#pragma once
#include "Screen.h"

// The manager of the screens must inherit from this class.
// Simulates the behaviour of a FSM, so the derived class
// must implement the SwitchTo method to disable the current
// screen and enable the next one.
class ScreensManager
{
	protected:
		Screen* _activeScreen;

	public:
		virtual void SwitchTo(const char* screenName) = 0;
};