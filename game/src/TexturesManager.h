#pragma once
#include <raylib.h>

// Static class with all the images of the game
// Call LoadTextures before starting the game
// and UnloadTextures when it finishes.
class TexturesManager
{
	public:
		static Texture2D Logo;
		static Texture2D Title;
		static Texture2D BodyPlayer;
		static Texture2D Turret;
		static Texture2D LeftHelicopter;
		static Texture2D RightHelicopter;
		static Texture2D Soldier;
		static Texture2D Dead;
		//
		static void LoadTextures();
		static void UnloadTextures();
};
