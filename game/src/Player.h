#pragma once
#include <raylib.h>
#include "Actor.h"

using namespace std;

// Class representing the actor of the game controlled 
// by the user
class Player : public Actor
{
	private:
		// Static body
		Texture2D _body;
		Rectangle _bodySource;
		Rectangle _bodyDest;
		// Rotating turret
		Texture2D _turret;
		Rectangle _turretSource;
		Rectangle _turretDest;
		float _rotation;
		float _rotationSpeed;
		//
		void CheckRotation();
		void CheckShooting();
		//
		int _score;
		// To shoot one at a time
		bool _canShoot;
		float _shotSpeed;
		//
		Music _rotateSound;
		bool _rotateSoundActive;

	public:
		// The rotation speed is in degrees/second
		Player(Rectangle player, Texture2D body,
			Texture2D turret, float rotationSpeed);
		void Update() override;
		void Draw() override;
		void IncreaseScore(int);
		int GetScore();
		Rectangle GetBody() override;
		~Player() override;
};

