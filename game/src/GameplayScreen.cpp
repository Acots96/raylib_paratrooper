#include "GameplayScreen.h"
#include <string>

GameplayScreen::GameplayScreen(Paratrooper* manager) : Screen(manager)
{
	Engine::ResetElapsedTime();
	_game = new GameManager();
	Engine::Instantiate(_game);
}

void GameplayScreen::Update()
{
	if (_game->GetState() != GameManager::State::Playing)
	{
		_manager->SwitchTo("Ending");
	}
}

void GameplayScreen::Draw()
{
	float w = _width;
	float h = _height;
	float y = h * 0.02f;

	int font = w / 40;

	// Score
	int len = MeasureText("SCORE: ", font);
	int posX = w * 0.1f;
	int incr = w * 0.02f;
	DrawText("SCORE: ", posX, y, font, WHITE);
	string s = to_string(_game->GetScore());
	DrawText(s.c_str(), posX + len + incr, y, font, SKYBLUE);

	// Landed
	len = MeasureText("LANDED: ", font);
	posX = w * 0.4f;
	DrawText("LANDED: ", posX, y, font, WHITE);
	s = to_string(_game->GetLanded());
	DrawText(s.c_str(), posX + len + incr, y, font, ORANGE);

	// Time
	len = MeasureText("TIME: ", font);
	posX = w * 0.7f;
	DrawText("TIME: ", posX, y, font, WHITE);
	string num = to_string(Engine::GetElapsedTime());
	string rounded = num.substr(0, num.find(".") + 4);
	DrawText(rounded.c_str(), posX + len + incr, y, font, LIGHTGRAY);
}

GameplayScreen::~GameplayScreen()
{
	Engine::Destroy(_game);
}
