#pragma once
#include <raylib.h>
#include "Actor.h"
#include <iostream>

using namespace std;

// Class representing the helicopter enemy
// which moves horizontally across the screen.
class Helicopter : public Actor
{
	private:
		Texture2D _body;
		Rectangle _bodySource;
		Rectangle _bodyDest;
		Rectangle _screen;
		Vector2 _position;
		float _speed;
		// Time interval to generate a soldier
		float _minTimeSoldier;
		float _maxTimeSoldier;
		float _deltaTime;
		float _speedSoldier;
		//
		Music _sound;

	public:
		// speed represents the horizontal velocity,
		// positive to go from left to right and 
		// negative otherwise
		Helicopter(Texture2D body, Vector2 startPos,
			float speed);
		void Update() override;
		void Draw() override;
		Rectangle GetBody() override;
		void Die();
		~Helicopter() override;
};