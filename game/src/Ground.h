#pragma once
#include "Actor.h"

// Class representing the Ground, which is the aim 
// for paratrooper soldiers.
// Has the counter of the soldiers landed.
class Ground : public Actor
{
	private:
		Rectangle _source;
		Rectangle _body;
		Color _color;
		Texture2D _texture;
		bool _isTexture;
		//
		int _landed;

	public:
		// Call this constructor to draw a simple rectangle
		// with the given color.
		Ground(Rectangle body, Color color);
		// Call this constructor if you want to draw a texture.
		Ground(Rectangle body, Texture2D texture, Color color);
		void Draw() override;
		Rectangle GetBody() override;
		void IncreaseLanded();
		int GetLanded();
};
