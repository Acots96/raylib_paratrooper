#pragma once
#include <iostream>
#include <raylib.h>
#include "Screen.h"
#include "Paratrooper.h"
#include "GameManager.h"

// Screen shown when the game is running.
// Creates the GameManager and draws the main information
// of the game in the top of it.
class GameplayScreen : public Screen
{
	private:
		GameManager* _game;

	public:
		GameplayScreen(Paratrooper*);
		void Update() override;
		void Draw() override;
		~GameplayScreen() override;
};
