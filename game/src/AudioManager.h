#pragma once
#include <raylib.h>

// Static class with all the sounds of the game
// Call LoadSounds before starting the game
// and UnloadSounds when it finishes.
class AudioManager
{
	public:
		static Sound Title;
		static Music Game;
		static Music Rotate;
		static Sound Shot;
		static Music Helicopter;
		static Sound Explosion;
		static Sound Jump;
		static Sound Land;
		static Sound EnemyDie;
		static Sound Loose;
		//
		static void LoadSounds();
		static void UnloadSounds();
};
