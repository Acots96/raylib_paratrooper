#pragma once
#include <iostream>
#include <raylib.h>
#include "Screen.h"
#include "Paratrooper.h"

// Screen shown when the player loses the game.
class EndingScreen : public Screen
{
	private:
		const char* _text;
		Vector2 _textPosition;

	public:
		EndingScreen(Paratrooper*);
		void Update() override;
		void Draw() override;
		~EndingScreen() override;
};
