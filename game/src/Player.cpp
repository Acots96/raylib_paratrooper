#include "Player.h"
#include <algorithm>
#include "Shot.h"
#include <iostream>
#include <stdio.h>
#include "AudioManager.h"
#include "GameManager.h"

Player::Player(Rectangle player, Texture2D body, Texture2D turret, float rotationSpeed) : Actor()
{
	// Static body
	_body = body;
	float pw = _body.width;
	float ph = _body.height;
	_bodySource = { 0, 0, pw, ph};
	_bodyDest = player;

	// Rotating turret
	_turret = turret;
	float tw = _turret.width;
	float th = _turret.height;
	_turretSource = { 0, 0, tw, th };
	_turretDest = { _bodyDest.x + pw / 2, _bodyDest.y + ph / 8, tw, th };
	_rotation = 0;
	_rotationSpeed = rotationSpeed;

	_score = 0;
	_canShoot = true;
	// Seconds to cross the screen horizontally
	float timeToCrossScreenWidth = 2.5f;
	_shotSpeed = (float)GetScreenWidth() * ((float)1 / timeToCrossScreenWidth);

	_rotateSoundActive = false;
	_rotateSound = AudioManager::Rotate;
	PlayMusicStream(_rotateSound);
	SetMusicVolume(_rotateSound, 0);
}

void Player::Update()
{
	CheckRotation();
	CheckShooting();

	UpdateMusicStream(_rotateSound);
}

// To rotate, the speed is multiplied by the time between frames,
// so the turret can rotate the correct amount of degrees each frame
void Player::CheckRotation()
{
	if (IsKeyDown(KEY_A))
	{
		if (!_rotateSoundActive)
		{
			SetMusicVolume(_rotateSound, 1);
			_rotateSoundActive = true;
		}
		_rotation = max(_rotation - _rotationSpeed * GetFrameTime(), (float)-90);
	}
	else if (IsKeyDown(KEY_D))
	{
		if (!_rotateSoundActive)
		{
			SetMusicVolume(_rotateSound, 1);
			_rotateSoundActive = true;
		}
		_rotation = min(_rotation + _rotationSpeed * GetFrameTime(), (float)90);
	}
	else
	{
		if (_rotateSoundActive)
		{
			SetMusicVolume(_rotateSound, 0);
			_rotateSoundActive = false;
		}
	}
}

Vector2 Normalize(Vector2 v)
{
	Vector2 result = { 0 };
	float length = sqrtf((v.x * v.x) + (v.y * v.y));

	if (length > 0)
	{
		float ilength = 1.0f / length;
		result.x = v.x * ilength;
		result.y = v.y * ilength;
	}

	return result;
}

// When the player shoots, the velocity of the shot is computed
// depending on the rotation of the turret
void Player::CheckShooting()
{
	if (IsKeyDown(KEY_SPACE) && _canShoot)
	{
		if (_score > 0)
		{
			// position
			Vector2 pos = { _turretDest.x, _turretDest.y };
			// velocity calculation
			auto rot = 2 * PI * (_rotation / 360);
			Vector2 vel = { sin(rot), -cos(rot) };
			vel = Normalize(vel);
			vel.x *= _shotSpeed;
			vel.y *= _shotSpeed;
			// shot
			auto shot = new Shot(pos, vel, _turretDest.width * 0.5f, this);
			Engine::Instantiate(shot);
			// Each shoot costs one point of the score, so decrease it.
			IncreaseScore(-1);
		}
		_canShoot = false;
	}
	else if (IsKeyUp(KEY_SPACE) && !_canShoot)
	{
		_canShoot = true;
	}
}

void Player::Draw()
{
	Vector2 t = { _turret.width / 2, _turret.height };
	DrawTexturePro(_turret, _turretSource, _turretDest, t, _rotation, WHITE);
	DrawTexturePro(_body, _bodySource, _bodyDest, { 0, 0 }, 0, WHITE);
}

void Player::IncreaseScore(int score)
{
	_score = max(_score + score, 0);
}

int Player::GetScore()
{
	return _score;
}

Rectangle Player::GetBody()
{
	return _bodyDest;
}

Player::~Player()
{
	StopMusicStream(_rotateSound);
}