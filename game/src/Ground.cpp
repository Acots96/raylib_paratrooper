#include "Ground.h"

Ground::Ground(Rectangle body, Texture2D texture, Color color) : Actor()
{
	_body = body;
	_texture = texture;
	_color = color;
	_source = { 0, 0, (float)texture.width, (float)texture.height };
	_isTexture = true;

	_landed = 0;
}
Ground::Ground(Rectangle body, Color color) : Actor()
{
	_body = body;
	_color = color;
	_isTexture = false;

	_landed = 0;
}

void Ground::Draw()
{
	if (_isTexture)
	{
		DrawTexturePro(_texture, _source, _body, { 0, 0 }, 0, _color);
	}
	else
	{
		DrawRectangle(_body.x, _body.y, _body.width, _body.height, _color);
	}
}

Rectangle Ground::GetBody()
{
	return _body;
}

void Ground::IncreaseLanded()
{
	_landed++;
}
int Ground::GetLanded()
{
	return _landed;
}
