#pragma once
#include <raylib.h>
#include "Actor.h"

// Class representing a paratrooper enemy
class Soldier : public Actor
{
	private:
		Texture2D _body;
		Rectangle _bodySource;
		Rectangle _bodyDest;
		Vector2 _position;
		float _speed;
		//
		bool _isDead;
		// Time while dead texture is active before disappearing
		float _deadTime;

	public:
		Soldier(Vector2 startPos, float speed);
		void Update() override;
		void Draw() override;
		Rectangle GetBody() override;
		void CollisionWith(Actor*) override;
		// Mark the soldier as dead to show its 
		// dead image for a certain time
		void Die();
		bool IsDead();
		~Soldier() override;
};
