#pragma once
#include <iostream>
#include "Actor.h"

class ScreensManager;

// Any screen in the game must inherit from this class.
// Simulates the behaviour of a FSM using a manager
class Screen : public Actor
{
	protected:
		int _width;
		int _height;
		ScreensManager* _manager;

	public:
		Screen(ScreensManager* manager)
		{
			_width = GetRenderWidth();
			_height = GetRenderHeight();
			_manager = manager;
		}
		virtual ~Screen() = default;
};