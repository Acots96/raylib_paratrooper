#include "Helicopter.h"
#include "Shot.h"
#include "TexturesManager.h"
#include "Soldier.h"
#include "AudioManager.h"
#include "GameManager.h"

float RandomTime(float min, float max);

Helicopter::Helicopter(Texture2D body, Vector2 startPos, float speed) : Actor()
{
	_body = body;
	_position = startPos;
	_speed = _speedSoldier = speed;

	_screen = { _position.x, _position.y, (float)GetRenderWidth(), (float)GetRenderHeight() };

	_bodySource = { 0, 0, (float)_body.width, (float)_body.height };
	_bodyDest = { _position.x, _position.y, (float)_body.width, (float)_body.height };

	_minTimeSoldier = 2;
	_maxTimeSoldier = 4;
	_deltaTime = RandomTime(_minTimeSoldier, _maxTimeSoldier);

	_sound = AudioManager::Helicopter;
	PlayMusicStream(_sound);
}

void Helicopter::Update()
{
	// This actor travels along the screen until hits another actor,
	// but if it doesn't, its position must be checked so it can be 
	// destroyed after leaving the screen.
	if (_bodyDest.x + _body.width < 0 || _bodyDest.y + _body.height < 0 ||
		_bodyDest.x > GetRenderWidth() || _bodyDest.y > GetRenderHeight())
	{
		Engine::Destroy(this);
	}
	else
	{
		_deltaTime -= GetFrameTime();
		if (_deltaTime <= 0)
		{
			// Can only create a soldier if it is within the screen.
			// It moves horizontally, so only X position check needed.
			if (_position.x > 0 && _position.x + _bodyDest.width < _screen.width)
			{
				Vector2 pos =
				{
					_position.x + _bodyDest.width / 2,
					_position.y + _bodyDest.height / 2,
				};
				Engine::Instantiate(new Soldier(pos, _speedSoldier));
			}
			_deltaTime = RandomTime(_minTimeSoldier, _maxTimeSoldier);
		}

		//

		_position.x += _speed * GetFrameTime();
		_bodyDest = { _position.x, _position.y, (float)_body.width, (float)_body.height };

		UpdateMusicStream(_sound);
	}
}

void Helicopter::Draw()
{
	DrawTexturePro(_body, _bodySource, _bodyDest, { 0, 0 }, 0, WHITE);
}

Rectangle Helicopter::GetBody()
{
	return _bodyDest;
}

void Helicopter::Die()
{
	Engine::Destroy(this);
}

Helicopter::~Helicopter()
{
	StopMusicStream(_sound);
	GameManager::RemoveActor(this);
}