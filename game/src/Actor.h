#pragma once
#include <raylib.h>
#include "Engine.h"

using namespace std;
class Engine;

// Any class that represents an actor of 
// your game must inherit from this class
// in order to be updated, drawn and 
// collision checked.
class Actor
{
	public:
		// It will be updated and drawn
		// only if is true.
		bool IsActive = true;
		//
		Actor() = default;
		// Called the first time right
		// after the initialization.
		virtual void Start() {};
		// Called each frame.
		// Perform here all the actions
		// like movement, input checking, etc.
		virtual void Update() {};
		// Called each frame.
		// Perform here all the drawings
		// needed for the character.
		virtual void Draw() {};
		// Called every time this actor
		// collides with other actors,
		// once per frame.
		virtual void CollisionWith(Actor*) {};
		// Rectangle used as the shape
		// to check the collisions
		// Default {0,0,0,0} means shapeless
		// and therefore will not collide.
		virtual Rectangle GetBody();
		virtual ~Actor() = default;
};
