#include "AudioManager.h"
#include "Paratrooper.h"
#include <iostream>
#include "TexturesManager.h"
#include "LogoScreen.h"
#include "TitleScreen.h"
#include "OptionsScreen.h"
#include "GameplayScreen.h"
#include "EndingScreen.h"

Paratrooper::Paratrooper()
{
	TexturesManager::LoadTextures();
	AudioManager::LoadSounds();

	_screens = {
		{ "Logo", 0 },
		{ "Title", 1 },
		{ "Options", 2 },
		{ "Gameplay", 3 },
		{ "Ending", 4 }
	};
	_activeScreen = nullptr;

	SwitchTo("Logo");
}

void Paratrooper::SwitchTo(const char* screenName)
{
	if (_activeScreen != nullptr)
		Engine::Destroy(_activeScreen);

	switch (_screens[screenName])
	{
		case 0:
			_activeScreen = new LogoScreen(this);
			break;
		case 1:
			_activeScreen = new TitleScreen(this);
			break;
		case 2:
			_activeScreen = new OptionsScreen(this);
			break;
		case 3:
			_activeScreen = new GameplayScreen(this);
			break;
		case 4:
			_activeScreen = new EndingScreen(this);
			break;
		default:
			break;
	}
	Engine::Instantiate(_activeScreen);
}

Paratrooper::~Paratrooper()
{
	TexturesManager::UnloadTextures();
	AudioManager::UnloadSounds();
}



///////////////////////////////////////


int main(void)
{
	const int screenWidth = 1366;
	const int screenHeight = 768;

	Engine::Start(screenWidth, screenHeight, 60, "Paratrooper!");
	Engine::Instantiate(new Paratrooper());
	//
	Engine::Execute();
}
