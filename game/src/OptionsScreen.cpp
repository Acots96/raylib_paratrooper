#include "OptionsScreen.h"

OptionsScreen::OptionsScreen(Paratrooper* manager) : Screen(manager)
{
	_text = "Do not allow enemy paratroopers to land\n"
		"on either side of your gun base. If seven\n"
		"paratroopers land on your base you will loose\n\n\n"
		"Press space to shoot\n"
		"Press 'A' to move the turret counterclockwise.\n"
		"Press 'D' to move the turret clockwise.\n\n\n"
		"Press 'O' to return to Title.";

	int textWidth = MeasureText(_text, _width / 45);
	_textPosition = { (float)(_width / 2 - textWidth / 2), _height * 0.1f };
}

void OptionsScreen::Update()
{
	if (IsKeyReleased(KEY_O))
	{
		_manager->SwitchTo("Title");
	}
}

void OptionsScreen::Draw()
{
	DrawText(_text, _textPosition.x, _textPosition.y, _width / 45, WHITE);
}
