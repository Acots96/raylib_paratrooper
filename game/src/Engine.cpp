#include "Actor.h"
#include "Engine.h"
#include <iostream>

using namespace std;

Engine* Engine::_instance;
double Engine::_elapsedTime;

void Engine::Start(int screenWidth, int screenHeight, int targetFPS, const char* title)
{
	// Initialize Raylib.
	InitWindow(screenWidth, screenHeight, title);
	SetTargetFPS(targetFPS);
	InitAudioDevice();

	// Create the instance.
	if (_instance != nullptr)
		delete _instance;
	_instance = new Engine();
}
Engine::Engine()
{
	_screen = { 0, 0, (float)GetRenderWidth(), (float)GetRenderHeight() };
	_elapsedTime = 0;
}

void Engine::Execute()
{
	while (!WindowShouldClose())
	{
		_instance->Update();
		_elapsedTime += GetFrameTime();
	}

	delete _instance;

	// Obsolete
	//StopSooundMulti();
	CloseAudioDevice();
	CloseWindow();
}

Engine::~Engine()
{
	while (!_actors.empty()) delete _actors.front(), _actors.pop_front();
}

void Engine::Instantiate(Actor* actor)
{
	_instance->AddActor(actor);
}

/// Check amongst all the existing actors before adding a 
/// new one so it will not be added twice or more.
void Engine::AddActor(Actor* actor)
{
	for (auto state = _actors.begin(); state != _actors.end(); ++state)
		if ((*state)->actor == actor) return;

	_actors.push_back(new ActorState(actor, true));
}

void Engine::Destroy(Actor* actor)
{
	_instance->RemoveActor(actor);
}

/// An actor is not directly removed when Engine::DestroyActor(actor)
/// is called, instead is marked as "dead" and it will be remove from 
/// the list later with all the other dead actors at the same time.
void Engine::RemoveActor(Actor* actor)
{
	for (auto state = _actors.begin(); state != _actors.end(); ++state)
	{
		if ((*state)->actor == actor)
		{
			(*state)->isAlive = false;
			break;
		}
	}
}

// Calls the Start/Update method of all the actors.
// Computes all the collisions between.
// Removes and deletes all the actors marked as "dead".
// Calls the Draw method of all the actors.
void Engine::Update()
{
	for (auto state = _actors.begin(); state != _actors.end(); ++state)
	{
		if (!(*state)->isAlive) continue;

		auto actor = (*state)->actor;
		if (actor->IsActive)
		{
			// If this is the first time of the actor -> Start method called.
			// From the second time until they die -> Update method called.
			if (!(*state)->isStarted)
			{
				(*state)->isStarted = true;
				actor->Start();
			}
			else actor->Update();
		}
	}

	CheckCollisions();

	RemoveDisabled();

	//

	BeginDrawing();
	ClearBackground(BLACK);

	for (auto state = _actors.begin(); state != _actors.end(); ++state)
	{
		if (!(*state)->isAlive || !(*state)->isStarted) continue;

		auto actor = (*state)->actor;
		if (actor->IsActive)
		{
			(*state)->actor->Draw();
		}
	}

	EndDrawing();
}

double Engine::GetElapsedTime()
{
	return _elapsedTime;
}
void Engine::ResetElapsedTime()
{
	_elapsedTime = 0;
}

// Iterates over all the actors and removes the ones makred as "dead".
void Engine::RemoveDisabled()
{
	auto actor = _actors.begin();
	while (actor != _actors.end())
	{
		auto a = *actor;
		if (a->actor == nullptr || !a->isAlive)
		{
			delete a->actor;
			actor = _actors.erase(actor);
		}
		else actor++;
	}
}

// Iterates over all the actors and checks if they are colliding
// with each other.
// The only collision avoided is one actor with itself.
void Engine::CheckCollisions()
{
	for (auto actor1 = _actors.begin(); actor1 != _actors.end(); ++actor1)
	{
		// Check if actor1 collides with any other actor.
		auto a1 = *actor1;
		if (!a1->isStarted || !a1->isAlive || !a1->actor->IsActive) continue;

		auto r1 = a1->actor->GetBody();
		for (auto actor2 = _actors.begin(); actor2 != _actors.end(); ++actor2)
		{
			auto a2 = *actor2;
			if (!a2->isStarted || !a2->isAlive || !a2->actor->IsActive || a1->actor == a2->actor)
				continue;

			if (CheckCollisionRecs(r1, a2->actor->GetBody()))
			{
				// If actor1 collides with actor2, notifies actor1
				a1->actor->CollisionWith(a2->actor);
			}
		}
	}
}
