#pragma once
#include <raylib.h>
#include <memory>
#include <list>
//#include "Actor.h"

using namespace std;
class Actor;

// Simulates the behaviour of a game engine
// implementing the basic functionalities
// such Update, Draw, Create/Destroy an Actor, etc.
// 
// It is implemented as a singleton to have one 
// single instance and public static functions
// so any actor can be added/removed from outside.
// Eventually, after closing the screen, the Engine
// instance will destroy itself and all the actors.
// 
// To initiate this engine:
//   Engine::Start(...);
//   ... (add actors here with Engine::CreateActor(...))
//   Engine::Execute();
class Engine
{
	// Helper class to hold an actor and its state
	// within the game:
	// - isAlive to check if can be removed.
	// - isStarted to call the Actor's Start method.
	class ActorState
	{
		public:
			Actor* actor;
			bool isAlive;
			bool isStarted;
			ActorState(Actor* actor, bool isAlive)
				: actor(actor), isAlive(isAlive), isStarted(false)
			{
			}
			~ActorState() = default;
	};

	private:
		static Engine* _instance;
		list<ActorState*> _actors;
		Rectangle _screen;
		Engine();
		void AddActor(Actor*);
		void RemoveActor(Actor*);
		void Update();
		void RemoveDisabled();
		void CheckCollisions();
		static double _elapsedTime;

	public:
		// First, call this function to
		// initialize the engine and Raylib.
		static void Start(int screenWidth, int screenHeight,
			int targetFPS, const char* title);
		// Second, add the actors (at least
		// one before calling Execute).
		// Can also be called while the engine
		// is being executed.
		static void Instantiate(Actor*);
		// Last, call Execute to run the engine
		// with all the actors you added.
		static void Execute();
		// Call this function to delete
		// an actor in the game.
		static void Destroy(Actor*);
		static double GetElapsedTime();
		static void ResetElapsedTime();
		Engine(Engine& other) = delete;
		void operator=(const Engine&) = delete;
		virtual ~Engine();
};
