#pragma once
#include <raylib.h>
#include "Screen.h"
#include "ScreensManager.h"
#include <memory>
#include <map>
#include "Actor.h"

using namespace std;

// Main class of the game.
// Holds an instance of the active screen and allows
// to change to another screen.
class Paratrooper : public Actor, public ScreensManager
{
	private:
		map<const char*, int> _screens;

	public:
		Paratrooper();
		void SwitchTo(const char* screenName) override;
		~Paratrooper() override;
};
