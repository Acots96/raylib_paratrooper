#include "GameManager.h"
#include "TexturesManager.h"
#include "AudioManager.h"
#include "Player.h"
#include "Helicopter.h"
#include "Ground.h"
#include <string>

using Txts = TexturesManager;
map<GameManager::Enemy, int> GameManager::Scores = {
	{ Enemy::Helicopter, 10 },
	{ Enemy::Soldier, 5 }
};
list<Actor*> GameManager::_actors;

float RandomTime(float min, float max);
Helicopter* GenerateHelicopter(float speed);

GameManager::GameManager() : Actor()
{
	auto w = (float)GetRenderWidth();
	auto h = (float)GetRenderHeight();
	_screen = { 0, 0, w, h };

	_actors = {};

	_maxLanded = 7;

	// Ground
	Rectangle ground = { 0, h * 0.95f, w, h * 0.05f };
	_ground = new Ground(ground, DARKBROWN);
	_actors.push_back(_ground);
	Engine::Instantiate(_ground);

	// Player
	float initialRotationSpeed = 270; // degrees per second
	float initialScore = 10;
	Rectangle playerBody = {
		w / 2 - Txts::BodyPlayer.width / 2,
		ground.y - Txts::BodyPlayer.height,
		Txts::BodyPlayer.width,
		Txts::BodyPlayer.height
	};
	_player = new Player(playerBody, Txts::BodyPlayer, Txts::Turret, initialRotationSpeed);
	_actors.push_back(_player);
	_player->IncreaseScore(initialScore);
	Engine::Instantiate(_player);

	// Helicopters
	_minTimeHelis = 2;
	_maxTimeHelis = 4;
	// Gets a random amount of time to generate 
	// a new helicopter given a certain interval.
	_deltaTime = RandomTime(_minTimeHelis, _maxTimeHelis);
	float secondsToCrossScreen = 6;
	_helicopterSpeed = w * (1 / secondsToCrossScreen);

	SetMusicVolume(AudioManager::Game, 0.5f);
	PlayMusicStream(AudioManager::Game);

	_state = State::Playing;
}

GameManager::~GameManager()
{
	for (auto it = _actors.begin(); it != _actors.end(); ++it)
		Engine::Destroy(*it);
	_actors.clear();
	StopMusicStream(AudioManager::Game);
}

void GameManager::Update()
{
	// Removes all the destroyed actors
	_actors.remove(NULL);

	if (_ground->GetLanded() >= _maxLanded)
	{
		_state = GameManager::State::Loose;
	}

	// After a certain amount of time,
	// a new helicopter is generated .
	_deltaTime -= GetFrameTime();
	if (_deltaTime <= 0)
	{
		auto h = GenerateHelicopter(_helicopterSpeed);
		_actors.push_back(h);
		Engine::Instantiate(h);
		_deltaTime = RandomTime(_minTimeHelis, _maxTimeHelis);
	}

	UpdateMusicStream(AudioManager::Game);
}

float RandomTime(float min, float max)
{
	float r = ((float)rand()) / (float)RAND_MAX;
	return min + r * (max - min);
}

Helicopter* GenerateHelicopter(float speed)
{
	// Randomly generates an helicopter and its start poisition.
	if (rand() % 2 == 0)
	{
		// Comes from left
		auto h = TexturesManager::LeftHelicopter;
		Vector2 v = { 0, (float)h.height + GetRenderHeight() * 0.1f };
		return new Helicopter(h, v, speed);
	}
	else
	{
		// Comes from right
		auto h = TexturesManager::RightHelicopter;
		Vector2 v = { GetRenderWidth() - 1, GetRenderHeight() * 0.1f };
		return new Helicopter(h, v, -speed);
	}
}

int GameManager::GetScore()
{
	return _player->GetScore();
}

int GameManager::GetLanded()
{
	return _ground->GetLanded();
}

GameManager::State GameManager::GetState()
{
	return _state;
}
void GameManager::SetState(GameManager::State state)
{
	_state = state;
}

void GameManager::AddActor(Actor* actor)
{
	_actors.push_back(actor);
}
void GameManager::RemoveActor(Actor* actor)
{
	_actors.remove(actor);
}
